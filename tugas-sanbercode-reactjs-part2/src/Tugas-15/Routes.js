import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import TemaContext, { tema } from "./TemaContext";
import Header from "./Header";

import Tugas9 from "../Tugas-9/soal1";
import Tugas10 from "../Tugas-10/soal2";
import Tugas11 from "../Tugas-11/Timer";
import Tugas12 from "../Tugas-12/PendaftaranBuah";
import Tugas13 from "../Tugas-13/DaftarBuah";
import Tugas14 from "../Tugas-14/DaftarBuah";

export default function App() {
  const [temaa, setTema] = useState(tema.dark);

  const toggleTema = () =>
    temaa === tema.dark ? setTema(tema.light) : setTema(tema.dark);

  return (
    <Router>
      <div>
        <TemaContext.Provider value={temaa}>
          <Header />
        </TemaContext.Provider>
        <Switch>
          <Route exact path="/tugas9">
            <Tugas9 />
          </Route>
          <Route exact path="/tugas10">
            <Tugas10 />
          </Route>
          <Route exact path="/tugas11">
            <Tugas11 />
          </Route>
          <Route exact path="/tugas12">
            <Tugas12 />
          </Route>
          <Route exact path="/tugas13">
            <Tugas13 />
          </Route>
          <Route exact path="/tugas14">
            <Tugas14 />
          </Route>
          <Route exact path="/tugas15">
            <TemaContext.Provider value={temaa}>
              <button onClick={toggleTema}>Ubah Tema</button>
            </TemaContext.Provider>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
