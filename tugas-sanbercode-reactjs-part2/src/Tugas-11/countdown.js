import React, { Component } from "react";
import "./countdown.css";

export default class Countdown extends Component {
  state = {
    angka: 100,
  };

  componentDidMount() {
    this.myInterval = setInterval(() => {
      const { angka } = this.state;

      if (angka > 0) {
        this.setState(({ angka }) => ({
          angka: angka - 1,
        }));
      }
      if (angka === 0) {
        clearInterval(this.myInterval);
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }

  render() {
    const { angka } = this.state;
    return (
      <div className="countdown">
        {angka === 0 ? "" : <p>hitung mundur: {angka}</p>}
      </div>
    );
  }
}
