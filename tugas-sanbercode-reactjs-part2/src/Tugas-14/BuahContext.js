import React, { useState, createContext } from "react";
// import axios from "axios";
import "./DaftarBuah.css";

export const BuahContext = createContext();

export const BuahProvider = (props) => {
  const [buah, setBuah] = useState(null);

  const [inputForm, setInputForm] = useState({
    // name: "",
    // lengthOfTime: 0,
    // id: null,
    name: "",
    price: "",
    weight: 0,
    id: null,
  });

  return (
    <BuahContext.Provider value={[buah, setBuah, inputForm, setInputForm]}>
      {props.children}
    </BuahContext.Provider>
  );
};
