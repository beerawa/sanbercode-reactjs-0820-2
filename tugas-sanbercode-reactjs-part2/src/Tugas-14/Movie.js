import React from "react";
import { MovieProvider } from "./MovieContext";
import MovieList from "./MovieList";
import MovieForm from "./MovieForm";

const Movie = () => {
  return (
    <MovieProvider>
      <div style={{ width: "40%", margin: "0 auto" }}>
        <MovieList />
        <br />
        <br />
        <MovieForm />
      </div>
    </MovieProvider>
  );
};

export default Movie;
