import React, { useContext, useEffect } from "react";
import { BuahContext } from "./BuahContext";
import axios from "axios";
import "./DaftarBuah.css";

const BuahForm = () => {
  const [buah, setBuah, inputForm, setInputForm] = useContext(BuahContext);

  useEffect(() => {
    if (buah === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          setBuah(res.data);
        });
    }
  }, [buah]);

  const handleSubmit = (event) => {
    event.preventDefault();
    // var newId = buah.length + 1;
    if (inputForm.id === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/fruits`, {
          name: inputForm.name,
          price: inputForm.price,
          weight: inputForm.weight,
        })
        .then((res) => {
          let data = res.data;
          setBuah([
            ...buah,
            {
              id: data.id,
              name: data.name,
              price: data.price,
              weight: data.weight,
            },
          ]);
          setInputForm({ id: null, name: "", price: "", weight: 0 });
        });
    } else {
      //   var singleBuah = buah.find((x) => x.id === inputForm.id);
      //   singleBuah.name = inputForm.name;
      //   singleBuah.price = inputForm.price;
      //   singleBuah.weight = inputForm.weight;
      //   setBuah([...buah]);
      axios
        .put(
          //   `http://backendexample.sanbercloud.com/api/contestants/${input.id}`,
          `http://backendexample.sanbercloud.com/api/fruits/${inputForm.id}`,
          {
            name: inputForm.name,
            price: inputForm.price,
            weight: inputForm.weight,
          }
        )
        .then((res) => {
          let newDataBuah = buah.map((x) => {
            if (x.id === inputForm.id) {
              x.name = inputForm.name;
              x.price = inputForm.price;
              x.weight = inputForm.weight;
            }
            return x;
          });
          setBuah(newDataBuah);
          setInputForm({ id: null, name: "", price: "", weight: 0 });
        });
    }
    // setInputForm({ name: "", price: "", weight: 0, id: null });
  };

  const handleChangeName = (event) => {
    setInputForm({ ...inputForm, name: event.target.value });
  };

  const handleChangePrice = (event) => {
    setInputForm({ ...inputForm, price: event.target.value });
  };

  const handleChangeWeight = (event) => {
    setInputForm({ ...inputForm, weight: event.target.value });
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <table>
          <tr>
            <td>
              <strong>Nama Buah : </strong>
            </td>
            <td>
              <input
                type="text"
                value={inputForm.name}
                onChange={handleChangeName}
              />
            </td>
          </tr>
          <tr>
            <td>
              <strong>Harga Buah : </strong>
            </td>
            <td>
              <input
                type="text"
                value={inputForm.price}
                onChange={handleChangePrice}
              />
            </td>
          </tr>
          <tr>
            <td>
              <strong>Berat Buah : </strong>
            </td>
            <td>
              <input
                type="number"
                value={inputForm.weight}
                onChange={handleChangeWeight}
              />
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <button>submit</button>
            </td>
          </tr>
        </table>
      </form>
    </>
  );
};

export default BuahForm;
