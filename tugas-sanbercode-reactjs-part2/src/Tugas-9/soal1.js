import React from "react";

const Soal1 = () => {
  return (
    <>
      <div className="container">
        <h1>Form Pembelian Buah</h1>
        <form action="#">
          <table className="table1" width="50%">
            <tr>
              <td>
                <label className="label-biasa" for="nama">
                  <b>Nama Pelanggan</b>
                </label>
              </td>
              <td>
                <input id="nama" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                <label className="label-item" for="daftar-item">
                  <b>Daftar Item</b>
                </label>
              </td>
              <td>
                <input
                  id="daftar-item"
                  name="daftar-item"
                  type="checkbox"
                  value="semangka"
                />
                Semangka
                <br />
                <input name="daftar-item" type="checkbox" value="jeruk" />
                Jeruk
                <br />
                <input name="daftar-item" type="checkbox" value="nanas" />
                Nanas
                <br />
                <input name="daftar-item" type="checkbox" value="salak" />
                Salak
                <br />
                <input name="daftar-item" type="checkbox" value="anggur" />
                Anggur
              </td>
            </tr>
            <tr>
              <td>
                <br />
                <button className="tombol-kirim">Kirim</button>
              </td>
            </tr>
          </table>
        </form>
      </div>
    </>
  );
};

export default Soal1;
