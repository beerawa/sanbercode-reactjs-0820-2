import React, { useContext, useEffect } from "react";
import { BuahContext } from "./BuahContext";
import axios from "axios";
import "./DaftarBuah.css";

const BuahList = () => {
  const [buah, setBuah, inputForm, setInputForm] = useContext(BuahContext);

  useEffect(() => {
    if (buah === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          setBuah(res.data);
        });
    }
  }, [buah]);

  const handleDelete = (event) => {
    // var idBuah = parseInt(event.target.value);
    // var newBuah = buah.filter((x) => x.id !== idBuah);
    // setBuah([...newBuah]);

    let idBuah = parseInt(event.target.value);
    axios
      .delete(
        // `http://backendexample.sanbercloud.com/api/contestants/${idPeserta}`
        `http://backendexample.sanbercloud.com/api/fruits/${idBuah}`
      )
      .then((res) => {
        let newDataBuah = buah.filter((x) => x.id !== idBuah);
        setBuah(newDataBuah);
      });
  };

  const handleEdit = (event) => {
    var idBuah = parseInt(event.target.value);
    var singleBuah = buah.find((x) => x.id === idBuah);
    setInputForm({
      ...inputForm,
      name: singleBuah.name,
      price: singleBuah.price,
      weight: singleBuah.weight,
      id: idBuah,
    });
  };

  return (
    <table>
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Harga</th>
          <th>Berat</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        {buah !== null &&
          buah.map((el, idx) => {
            return (
              <tr key={el.id}>
                <td>{idx + 1}</td>
                <td>{el.name}</td>
                <td>{el.price}</td>
                <td>{el.weight / 1000} kg</td>
                <td>
                  <button
                    value={el.id}
                    style={{ marginRight: "10px" }}
                    onClick={handleEdit}
                  >
                    Edit
                  </button>
                  <button value={el.id} onClick={handleDelete}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export default BuahList;
