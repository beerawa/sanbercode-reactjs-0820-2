import React from "react";

let dataHargaBuah = [
  { nama: "Semangka", harga: 10000, berat: 1000 },
  { nama: "Anggur", harga: 40000, berat: 500 },
  { nama: "Strawberry", harga: 30000, berat: 400 },
  { nama: "Jeruk", harga: 30000, berat: 1000 },
  { nama: "Mangga", harga: 30000, berat: 500 },
];

const Soal2 = () => {
  return (
    <>
      <div className="container-2">
        <h1>Tabel Harga Buah</h1>
        <br></br>
        <table className="table-soal2">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
          </thead>
          <tbody>
            {dataHargaBuah.map((ele) => {
              return (
                <tr>
                  <td>{ele.nama}</td>
                  <td>{ele.harga}</td>
                  <td>{ele.berat / 1000} kg</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Soal2;
