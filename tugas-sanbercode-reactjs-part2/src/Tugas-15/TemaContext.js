import React from "react";

export const tema = {
  dark: {
    color: "white",
    background: "black",
    listStyle: "none",
    display: "inline-block",
    marginLeft: "10px",
  },
  light: {
    color: "black",
    background: "white",
    listStyle: "none",
    display: "inline-block",
    marginLeft: "10px",
  },
};

const TemaContext = React.createContext(tema.dark);

export default TemaContext;
