import React, { Component } from "react";

class PendaftaranBuah extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // pesertaLomba: ["Budi", "Susi", "Anto", "Lala"],
      inputNama: "",
      inputHarga: "",
      inputBerat: 0,
      index: -1,
      dataHargaBuah: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],
    };
    this.submitForm = this.submitForm.bind(this);
    this.deleteBuah = this.deleteBuah.bind(this);
  }

  submitForm(event) {
    event.preventDefault();
    // if(this.state.inputNama.replace)
    var index = this.state.index;
    var obyek = {
      nama: this.state.inputNama,
      harga: this.state.inputHarga,
      berat: this.state.inputBerat,
    };
    if (index === -1) {
      this.setState({
        // pesertaLomba: [...this.state.pesertaLomba, this.state.inputNama],
        dataHargaBuah: [...this.state.dataHargaBuah, obyek],
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
      });
    } else {
      var newHargaBuah = this.state.dataHargaBuah;
      newHargaBuah[index] = obyek;

      this.setState({
        dataHargaBuah: [...newHargaBuah],
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
        index: -1,
      });
    }
    obyek = {};
  }

  changeInputName = (event) => {
    var value = event.target.value;
    this.setState({ inputNama: value });
  };
  changeInputHarga = (event) => {
    var value = event.target.value;
    this.setState({ inputHarga: value });
  };
  changeInputBerat = (event) => {
    var value = event.target.value;
    this.setState({ inputBerat: value });
  };

  editForm = (event) => {
    var index = event.target.value;
    var hargaBuah = this.state.dataHargaBuah[index];
    this.setState({
      inputNama: hargaBuah.nama,
      inputHarga: hargaBuah.harga,
      inputBerat: hargaBuah.berat,
      index: index,
    });
  };

  deleteBuah(event) {
    var index = event.target.value;
    var newHargaBuah = this.state.dataHargaBuah;
    newHargaBuah.splice(index, 1);
    this.setState({
      pesertaLomba: [...newHargaBuah],
      inputNama: "",
      inputHarga: "",
      inputBerat: "",
      index: -1,
    });
  }

  render() {
    return (
      <>
        <div>
          <h1>Tabel Harga Buah</h1>

          <table
            style={{ border: "1px solid", width: "40%", margin: "0 auto" }}
          >
            <thead style={{ background: "#aaa" }}>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody style={{ background: "coral" }}>
              {this.state.dataHargaBuah.map((el, index) => {
                return (
                  <tr key={index}>
                    <td>{el.nama}</td>
                    <td>{el.harga}</td>
                    <td>{el.berat / 1000} kg</td>
                    <td>
                      <button value={index} onClick={this.editForm}>
                        Edit
                      </button>
                      <button value={index} onClick={this.deleteBuah}>
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>

          <br></br>
          <br></br>

          <h1>Form Input Buah</h1>

          <form onSubmit={this.submitForm}>
            <table
              style={{ border: "1px solid", width: "40%", margin: "0 auto" }}
            >
              <tbody style={{ background: "coral" }}>
                <tr>
                  <td>
                    <strong>Nama</strong>
                  </td>
                  <td>
                    <input
                      type="text"
                      value={this.state.inputNama}
                      onChange={this.changeInputName}
                    ></input>
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Harga</strong>
                  </td>
                  <td>
                    <input
                      type="text"
                      value={this.state.inputHarga}
                      onChange={this.changeInputHarga}
                    ></input>
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Berat</strong>
                  </td>
                  <td>
                    <input
                      type="text"
                      value={this.state.inputBerat}
                      onChange={this.changeInputBerat}
                    ></input>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <button>Save</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </form>

          {/* <br></br>
          <br></br>
          <form onSubmit={this.submitForm}>
            <strong>Nama</strong>
            <input
              type="text"
              value={this.state.inputNama}
              onChange={this.changeInputName}
            ></input>
            <br></br>
            <strong>Harga</strong>
            <input
              type="text"
              value={this.state.inputHarga}
              onChange={this.changeInputHarga}
            ></input>
            <br></br>
            <strong>Berat</strong>
            <input
              type="text"
              value={this.state.inputBerat}
              onChange={this.changeInputBerat}
            ></input>
            <br></br>
            <button>Save</button>
          </form> */}
        </div>
      </>
    );
  }
}

export default PendaftaranBuah;
