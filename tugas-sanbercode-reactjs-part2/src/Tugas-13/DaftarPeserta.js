import React, { useState, useEffect } from "react";
import axios from "axios";

const DaftarPeserta = () => {
  const [dataPeserta, setDataPeserta] = useState(null);
  //   const [inputname, setInputname] = useState("");
  const [input, setInput] = useState({ name: "", id: null });

  useEffect(() => {
    if (dataPeserta === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/contestants`)
        .then((res) => {
          setDataPeserta(res.data);
        });
    }
  }, [dataPeserta]);

  const submitForm = (event) => {
    event.preventDefault();
    // let index = this.state.index;
    if (input.id === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/contestants`, {
          name: input.name,
        })
        .then((res) => {
          let data = res.data;
          setDataPeserta([...dataPeserta, { id: data.id, name: data.name }]);
          setInput({ id: null, name: "" });
        });
      //   this.setState({
      //     pesertaLomba: [...this.state.pesertaLomba, this.state.inputname],
      //     inputname: "",
      //   });
    } else {
      axios
        .put(
          `http://backendexample.sanbercloud.com/api/contestants/${input.id}`,
          {
            name: input.name,
          }
        )
        .then((res) => {
          //   let data = res.data;
          let newDataPeserta = dataPeserta.map((x) => {
            if (x.id === input.id) {
              x.name = input.name;
            }
            return x;
          });
          setDataPeserta(newDataPeserta);
          setInput({ id: null, name: "" });
        });
      //   let newPesertaLomba = this.state.pesertaLomba;
      //   newPesertaLomba[index] = this.state.inputname;

      //   this.setState({
      //     pesertaLomba: [...newPesertaLomba],
      //     inputname: "",
      //     index: -1,
      //   });
    }
  };

  const deletePeserta = (event) => {
    let idPeserta = parseInt(event.target.value);
    axios
      .delete(
        `http://backendexample.sanbercloud.com/api/contestants/${idPeserta}`
      )
      .then((res) => {
        let newDataPeserta = dataPeserta.filter((x) => x.id !== idPeserta);
        setDataPeserta(newDataPeserta);
      });
    // let newPesertaLomba = this.state.pesertaLomba;
    // newPesertaLomba.splice(index, 1);
    // this.setState({
    //   pesertaLomba: [...newPesertaLomba],
    //   inputname: "",
    //   index: -1,
    // });
  };

  const changeInputname = (event) => {
    let value = event.target.value;
    // this.setState({ inputname: value });
    setInput({ ...input, name: value });
  };

  const editForm = (event) => {
    let idPeserta = parseInt(event.target.value);
    // let namePeserta = this.state.pesertaLomba[index];
    let peserta = dataPeserta.find((x) => x.id === idPeserta);
    setInput({ id: idPeserta, name: peserta.name });
  };

  return (
    <>
      <div style={{ width: "70vw", margin: "0 auto" }}>
        <h1 style={{ textAlign: "center" }}>Daftar Peserta Lomba</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>name</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {dataPeserta !== null &&
              dataPeserta.map((item, index) => {
                return (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>
                      <button
                        value={item.id}
                        style={{ marginRight: "5px" }}
                        onClick={editForm}
                      >
                        Edit
                      </button>
                      <button value={item.id} onClick={deletePeserta}>
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <br></br>
        <br></br>
        <form style={{ textAlign: "center" }} onSubmit={submitForm}>
          <strong style={{ marginRight: "10px" }}>name</strong>
          <input
            required
            type="text"
            value={input.name}
            onChange={changeInputname}
          ></input>
          <button>Save</button>
        </form>
      </div>
    </>
  );
};

export default DaftarPeserta;
