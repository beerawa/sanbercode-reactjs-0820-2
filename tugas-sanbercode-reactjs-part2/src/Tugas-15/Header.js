import React, { useContext } from "react";
import TemaContext from "./TemaContext";
import { Link } from "react-router-dom";

const Header = () => {
  const tema = useContext(TemaContext);
  return (
    <div>
      <nav style={tema}>
        <ul>
          <li style={tema}>
            <Link to="/tugas9">Tugas9</Link>
          </li>
          <li style={tema}>
            <Link to="/tugas10">Tugas10</Link>
          </li>
          <li style={tema}>
            <Link to="/tugas11">Tugas11</Link>
          </li>
          <li style={tema}>
            <Link to="/tugas12">Tugas12</Link>
          </li>
          <li style={tema}>
            <Link to="/tugas13">Tugas13</Link>
          </li>
          <li style={tema}>
            <Link to="/tugas14">Tugas14</Link>
          </li>
          <li style={tema}>
            <Link to="/tugas15">Tugas15</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Header;
