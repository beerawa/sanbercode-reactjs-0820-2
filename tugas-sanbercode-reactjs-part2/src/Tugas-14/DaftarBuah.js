import React, { useState, useEffect } from "react";
import axios from "axios";
import "./DaftarBuah.css";

const DaftarBuah = () => {
  const [dataBuah, setDataBuah] = useState(null);
  //   const [inputname, setInputname] = useState("");
  const [input, setInput] = useState({
    name: "",
    price: "",
    weight: 0,
    id: null,
  });

  useEffect(() => {
    if (dataBuah === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          setDataBuah(res.data);
        });
    }
  }, [dataBuah]);

  const submitForm = (event) => {
    event.preventDefault();
    if (input.id === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/fruits`, {
          name: input.name,
          price: input.price,
          weight: input.weight,
        })
        .then((res) => {
          let data = res.data;
          setDataBuah([
            ...dataBuah,
            {
              id: data.id,
              name: data.name,
              price: data.price,
              weight: data.weight,
            },
          ]);
          setInput({ id: null, name: "", price: "", weight: 0 });
        });
    } else {
      axios
        .put(
          //   `http://backendexample.sanbercloud.com/api/contestants/${input.id}`,
          `http://backendexample.sanbercloud.com/api/fruits/${input.id}`,
          {
            name: input.name,
            price: input.price,
            weight: input.weight,
          }
        )
        .then((res) => {
          let newDataBuah = dataBuah.map((x) => {
            if (x.id === input.id) {
              x.name = input.name;
              x.price = input.price;
              x.weight = input.weight;
            }
            return x;
          });
          setDataBuah(newDataBuah);
          setInput({ id: null, name: "", price: "", weight: 0 });
        });
    }
  };

  const deleteBuah = (event) => {
    let idBuah = parseInt(event.target.value);
    axios
      .delete(
        // `http://backendexample.sanbercloud.com/api/contestants/${idPeserta}`
        `http://backendexample.sanbercloud.com/api/fruits/${idBuah}`
      )
      .then((res) => {
        let newDataBuah = dataBuah.filter((x) => x.id !== idBuah);
        setDataBuah(newDataBuah);
      });
  };

  const changeInputName = (event) => {
    let value = event.target.value;
    setInput({ ...input, name: value });
  };
  const changeInputPrice = (event) => {
    let value = event.target.value;
    setInput({ ...input, price: value });
  };
  const changeInputWeight = (event) => {
    let value = event.target.value;
    setInput({ ...input, weight: value });
  };

  const editForm = (event) => {
    let idBuah = parseInt(event.target.value);
    let buah = dataBuah.find((x) => x.id === idBuah);
    setInput({
      id: idBuah,
      name: buah.name,
      price: buah.price,
      weight: buah.weight,
    });
  };

  return (
    <>
      <h1>Daftar Harga Buah</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {dataBuah !== null &&
            dataBuah.map((item, index) => {
              return (
                <tr key={item.id}>
                  <td>{index + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>{item.weight / 1000} kg</td>
                  <td>
                    <button onClick={editForm} value={item.id}>
                      Edit
                    </button>
                    &nbsp;
                    <button onClick={deleteBuah} value={item.id}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
      {/* Form */}
      <h1>Form Daftar Harga Buah</h1>
      <div style={{ width: "50%", margin: "0 auto", display: "block" }}>
        <div style={{ border: "1px solid #aaa", padding: "20px" }}>
          <form onSubmit={submitForm}>
            <label style={{ float: "left" }}>Nama:</label>
            <input
              required
              style={{ float: "right" }}
              type="text"
              name="name"
              value={input.name}
              onChange={changeInputName}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Harga:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="harga"
              value={input.price}
              onChange={changeInputPrice}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Berat (dalam gram):</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="berat"
              value={input.weight}
              onChange={changeInputWeight}
            />
            <br />
            <br />
            <div style={{ width: "100%", paddingBottom: "20px" }}>
              <button style={{ float: "right" }}>submit</button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default DaftarBuah;
