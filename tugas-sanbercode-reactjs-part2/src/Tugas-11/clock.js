import React, { Component } from "react";
import "./clock.css";
import Countdown from "./countdown";

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleTimeString(),
      angka: 100,
    };
  }

  componentDidMount() {
    this.intervalID = setInterval(() => this.updateClock(), 1000);
    this.myInterval = setInterval(() => {
      const { angka } = this.state;

      if (angka > 0) {
        this.setState(({ angka }) => ({
          angka: angka - 1,
        }));
      }
      if (angka === 0) {
        clearInterval(this.myInterval);
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
    clearInterval(this.myInterval);
  }

  updateClock() {
    this.setState({
      time: new Date().toLocaleTimeString(),
    });
  }
  render() {
    const { angka } = this.state;
    return (
      <div className="Time">
        {angka === 0 ? (
          ""
        ) : (
          <ul>
            <li>
              Sekarang jam : {this.state.time} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </li>
            <li>
              <Countdown />
            </li>
          </ul>
        )}
      </div>
    );
  }
}
export default Clock;
