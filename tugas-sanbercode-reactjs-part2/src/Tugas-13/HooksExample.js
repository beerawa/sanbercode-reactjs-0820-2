import React, { useState, useEffect } from "react";

const HooksExample = () => {
  const [name, setName] = useState("");
  const [count, setCount] = useState(0);

  const showName = (event) => {
    let getName = event.target.value;
    setName(getName);
  };

  useEffect(() => {
    if (count > 5) {
      document.title = `You clicked ${count} times`;
    }
  }, [count]);

  return (
    <>
      <div style={{ marginTop: "20px", textAlign: "center" }}>
        <input onChange={showName} value={name} />
        <h1>{name}</h1>
        <br></br>
        <br></br>
        <h1>{count}</h1>
        <button onClick={() => setCount(count + 1)}>
          Click to added Count
        </button>
      </div>
    </>
  );
};

export default HooksExample;
